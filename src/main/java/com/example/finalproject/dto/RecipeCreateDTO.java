package com.example.finalproject.dto;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecipeCreateDTO {

    private Long id;

    private String description;

    private Integer preparationTime;

    private Difficulty difficulty;

    private String instructions;

}
