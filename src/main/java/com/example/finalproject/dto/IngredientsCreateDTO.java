package com.example.finalproject.dto;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class IngredientsCreateDTO {

    private Long id;

    private String name;

    private Long kcal;
}
