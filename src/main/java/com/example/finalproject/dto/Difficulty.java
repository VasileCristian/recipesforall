package com.example.finalproject.dto;

public enum Difficulty {
    EASY, MEDIUM, HARD
}

