package com.example.finalproject.dto;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserCreateDTO {

    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private Integer age;

    private String password;
}
