package com.example.finalproject.dto;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecipeDTO {

    private String description;

    private Integer preparationTime;

    private Difficulty difficulty;

    private String instructions;
}
