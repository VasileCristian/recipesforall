package com.example.finalproject.dto;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class IngredientsDTO {

    private String name;

    private Long kcal;
}
