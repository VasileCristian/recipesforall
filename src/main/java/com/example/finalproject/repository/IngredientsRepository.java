package com.example.finalproject.repository;

import com.example.finalproject.entity.Ingredients;
import org.springframework.data.repository.CrudRepository;

public interface IngredientsRepository extends CrudRepository<Ingredients, Long> {

}
