package com.example.finalproject.controller;

import com.example.finalproject.dto.IngredientsCreateDTO;
import com.example.finalproject.dto.IngredientsDTO;
import com.example.finalproject.service.IngredientsService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("api/ingredients")
public class IngredientsController {

    private final IngredientsService ingredientsService;

    @PostMapping
    public ResponseEntity<IngredientsDTO> addIngredients(@RequestBody IngredientsCreateDTO ingredientsCreateDTO){
        return ResponseEntity.ok(ingredientsService.addIngredient(ingredientsCreateDTO));
    }
    @GetMapping
    public ResponseEntity<List<IngredientsDTO>> getIngredients() {
        return ResponseEntity.ok(ingredientsService.getIngredient());
    }

    @PutMapping("/{id}")
    public ResponseEntity<IngredientsDTO> updateIngredients(@RequestBody IngredientsCreateDTO ingredientsCreateDTO, @PathVariable Long id) {
        return ResponseEntity.ok(ingredientsService.updateIngredient(ingredientsCreateDTO, id));
    }
    @DeleteMapping("/{id}")
    public void deleteIngredient(@PathVariable Long id) {
        ingredientsService.deleteIngredient(id);
    }
}
