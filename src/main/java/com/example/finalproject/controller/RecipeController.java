package com.example.finalproject.controller;

import com.example.finalproject.dto.RecipeCreateDTO;
import com.example.finalproject.dto.RecipeDTO;
import com.example.finalproject.service.RecipeService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("api/recipe")
public class RecipeController {

    private final RecipeService recipeService;
    @PostMapping
    public ResponseEntity<RecipeDTO> addRecipe(@RequestBody RecipeCreateDTO recipeCreateDTO) {
        return ResponseEntity.ok(recipeService.addRecipe(recipeCreateDTO));
    }

    @GetMapping
    public ResponseEntity<List<RecipeDTO>> getRecipes() {
        return ResponseEntity.ok(recipeService.getRecipes());
    }

    @PutMapping("/{id}")
    public ResponseEntity<RecipeDTO> updateRecipe(@RequestBody RecipeCreateDTO recipeCreateDTO, @PathVariable Long id) {
        return ResponseEntity.ok(recipeService.updateRecipe(recipeCreateDTO, id));
    }

    @DeleteMapping("/{id}")
    public void deleteRecipe(@PathVariable Long id) {
        recipeService.deleteRecipe(id);
    }
}
