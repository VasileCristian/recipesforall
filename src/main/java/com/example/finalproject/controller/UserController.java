package com.example.finalproject.controller;

import com.example.finalproject.dto.UserCreateDTO;
import com.example.finalproject.dto.UserDTO;
import com.example.finalproject.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("api/user")
public class UserController {

    private final UserService userService;

    @PostMapping
    public ResponseEntity<UserDTO> addUser(@RequestBody UserCreateDTO userCreateDTO){
        return ResponseEntity.ok(userService.addUser(userCreateDTO));
    }

    @GetMapping
    public ResponseEntity <List<UserDTO>> getUser() {
        return ResponseEntity.ok(userService.getUsers());
    }
    @PutMapping("/{id}")
    public ResponseEntity<UserDTO> updateUser(@RequestBody UserCreateDTO userCreateDTO, @PathVariable Long id) {
        return ResponseEntity.ok(userService.updateUser(userCreateDTO, id));
    }
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }
}

