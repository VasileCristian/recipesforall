package com.example.finalproject.service.impl;

import com.example.finalproject.dto.RecipeCreateDTO;
import com.example.finalproject.dto.RecipeDTO;
import com.example.finalproject.entity.Recipe;
import com.example.finalproject.repository.RecipeRepository;
import com.example.finalproject.service.RecipeService;
import com.example.finalproject.service.mapper.RecipeMapper;
import com.google.common.collect.ImmutableList;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
abstract class RecipeServiceImpl implements RecipeService {

    private RecipeRepository recipeRepository;

    private RecipeMapper recipeMapper;

    public RecipeServiceImpl(RecipeRepository recipeRepository, RecipeMapper recipeMapper) {
        this.recipeRepository = recipeRepository;
        this.recipeMapper = recipeMapper;
    }

    public RecipeDTO addRecipe(RecipeCreateDTO recipe) {
        Recipe toBeSaved = recipeMapper.toEntity(recipe);
        Recipe saved = recipeRepository.save(toBeSaved);
        return recipeMapper.mapToDto(saved);
    }

    public List<RecipeDTO> getRecipe() {

        List<Recipe> allRecipes = ImmutableList.copyOf(recipeRepository.findAll());
        List<RecipeDTO> allRecipeDTO = new ArrayList<>();
        for (Recipe u : allRecipes) {
            RecipeDTO dto = recipeMapper.mapToDto(u);
            allRecipeDTO.add(dto);
        }
        return allRecipeDTO;
    }

    public RecipeDTO updateRecipe(RecipeCreateDTO recipeCreateDTO, Long id) {
        recipeRepository.findById(id).orElseThrow(NoSuchElementException::new);
        recipeCreateDTO.setId(id);
        Recipe toBeSaved = recipeMapper.toEntity(recipeCreateDTO);
        Recipe saved = recipeRepository.save(toBeSaved);
        return recipeMapper.mapToDto(saved);
    }

    public void deleteRecipe(Long id) {
        recipeRepository.findById(id).orElseThrow(NoSuchElementException::new);
        recipeRepository.deleteById(id);
    }
}
