package com.example.finalproject.service.impl;

import com.example.finalproject.dto.UserCreateDTO;
import com.example.finalproject.dto.UserDTO;
import com.example.finalproject.entity.User;
import com.example.finalproject.repository.UserRepository;
import com.example.finalproject.service.UserService;
import com.example.finalproject.service.mapper.UserMapper;
import com.google.common.collect.ImmutableList;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private UserMapper userMapper;

    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public UserDTO addUser(UserCreateDTO user) {
        User toBeSaved = userMapper.toEntity(user);
        User saved = userRepository.save(toBeSaved);
        return userMapper.mapToDto(saved);
    }

    @Override
    public List<UserDTO> getUsers() {
        // retrive all users (Entity)
        List<User> allUsers = ImmutableList.copyOf(userRepository.findAll());

//        List<UserDTO> allUsersDTO = new ArrayList<>();
//        for(User u: allUsers) {
//            UserDTO dto = userMapper.mapToDto(u);
//            allUsersDTO.add(dto);
//        }


        // map each user u to a dto and then collect the result
        List<UserDTO> allUsersDTO = allUsers.stream()
                .map(u -> {
                    return userMapper.mapToDto(u);
                })
                .collect(Collectors.toList());

        return allUsersDTO;
    }

    @Override
    public UserDTO updateUser(UserCreateDTO userCreateDTO, Long id) {
        userRepository.findById(id).orElseThrow(NoSuchElementException::new);
        userCreateDTO.setId(id);
        User toBeSaved = userMapper.toEntity(userCreateDTO);
        User saved = userRepository.save(toBeSaved);
        return userMapper.mapToDto(saved);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.findById(id).orElseThrow(NoSuchElementException::new);
        userRepository.deleteById(id);
    }
}
