package com.example.finalproject.service.impl;

import com.example.finalproject.dto.IngredientsCreateDTO;
import com.example.finalproject.dto.IngredientsDTO;
import com.example.finalproject.entity.Ingredients;
import com.example.finalproject.repository.IngredientsRepository;
import com.example.finalproject.service.IngredientsService;
import com.example.finalproject.service.mapper.IngredientsMapper;
import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

abstract class IngredientsServiceImpl implements IngredientsService {

    private IngredientsRepository ingredientsRepository;

    private IngredientsMapper ingredientsMapper;

    public IngredientsServiceImpl(IngredientsRepository ingredientsRepository, IngredientsMapper ingredientsMapper) {
        this.ingredientsRepository = ingredientsRepository;
        this.ingredientsMapper = ingredientsMapper;
    }

    public IngredientsDTO addIngredients(IngredientsCreateDTO ingredients) {
        Ingredients toBeSaved = ingredientsMapper.toEntity(ingredients);
        Ingredients saved = ingredientsRepository.save(toBeSaved);
        return ingredientsMapper.mapToDto(saved);
    }

    public List<IngredientsDTO> getIngredient() {

        List<Ingredients> allIngredients = ImmutableList.copyOf(ingredientsRepository.findAll());

        List<IngredientsDTO> allIngredientsDTO = allIngredients.stream()
                .map(u -> {
                    return ingredientsMapper.mapToDto(u);
                })
                .collect(Collectors.toList());

        return allIngredientsDTO;
    }

    public IngredientsDTO updateIngredient(IngredientsCreateDTO ingredientsCreateDTO, Long id) {
        ingredientsRepository.findById(id).orElseThrow(NoSuchElementException::new);
        ingredientsCreateDTO.setId(id);
        Ingredients toBeSaved = ingredientsMapper.toEntity(ingredientsCreateDTO);
        Ingredients saved = ingredientsRepository.save(toBeSaved);
        return ingredientsMapper.mapToDto(saved);
    }

    public void deleteIngredient(Long id) {
        ingredientsRepository.findById(id).orElseThrow(NoSuchElementException::new);
        ingredientsRepository.deleteById(id);
    }
}
