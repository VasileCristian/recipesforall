package com.example.finalproject.service;

import com.example.finalproject.dto.RecipeCreateDTO;
import com.example.finalproject.dto.RecipeDTO;

import java.util.List;

public interface RecipeService {

    RecipeDTO addRecipe(RecipeCreateDTO recipe);

    List<RecipeDTO> getRecipes();

    RecipeDTO updateRecipe(RecipeCreateDTO recipe, Long id);

    void deleteRecipe(Long id);

}
