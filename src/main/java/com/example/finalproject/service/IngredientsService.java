package com.example.finalproject.service;

import com.example.finalproject.dto.IngredientsCreateDTO;
import com.example.finalproject.dto.IngredientsDTO;

import java.util.List;

public interface IngredientsService {

    IngredientsDTO addIngredient(IngredientsCreateDTO ingredient);

    List<IngredientsDTO> getIngredient();

    IngredientsDTO updateIngredient(IngredientsCreateDTO ingredient, Long id);

    void deleteIngredient(Long id);
}
