package com.example.finalproject.service.mapper;

import com.example.finalproject.dto.IngredientsCreateDTO;
import com.example.finalproject.dto.IngredientsDTO;
import com.example.finalproject.entity.Ingredients;
import org.springframework.stereotype.Component;

@Component
public class IngredientsMapper {

    public Ingredients toEntity(IngredientsCreateDTO createDTO) {

        return Ingredients.builder()
                .id(createDTO.getId())
                .name(createDTO.getName())
                .kcal(createDTO.getKcal())
                .build();
    }

    public IngredientsDTO mapToDto(Ingredients ingredients) {

        return IngredientsDTO.builder()
                .name(ingredients.getName())
                .kcal(ingredients.getKcal())
                .build();
    }
}
