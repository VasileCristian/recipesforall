package com.example.finalproject.service.mapper;

import com.example.finalproject.dto.UserCreateDTO;
import com.example.finalproject.dto.UserDTO;
import com.example.finalproject.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public User toEntity(UserCreateDTO createDTO) {

        return User.builder()
                .id(createDTO.getId())
                .firstName(createDTO.getFirstName())
                .lastName(createDTO.getLastName())
                .email(createDTO.getEmail())
                .age(createDTO.getAge())
                .password(createDTO.getPassword())
                .build();
    }

    public UserDTO mapToDto(User user) {

        return UserDTO.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .age(user.getAge())
                .build();
    }
}
