package com.example.finalproject.service.mapper;

import com.example.finalproject.dto.RecipeCreateDTO;
import com.example.finalproject.dto.RecipeDTO;
import com.example.finalproject.entity.Recipe;
import org.springframework.stereotype.Component;

@Component
public class RecipeMapper {

    public Recipe toEntity(RecipeCreateDTO createDTO) {

        return Recipe.builder()
                .id(createDTO.getId())
                .description(createDTO.getDescription())
                .preparationTime(createDTO.getPreparationTime())
                .difficulty(createDTO.getDifficulty())
                .instructions(createDTO.getInstructions())
                .build();
    }

    public RecipeDTO mapToDto(Recipe recipe) {

        return RecipeDTO.builder()
                .description(recipe.getDescription())
                .preparationTime(recipe.getPreparationTime())
                .difficulty(recipe.getDifficulty())
                .instructions(recipe.getInstructions())
                .build();
    }
}
