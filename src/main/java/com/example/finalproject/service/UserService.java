package com.example.finalproject.service;

import com.example.finalproject.dto.UserCreateDTO;
import com.example.finalproject.dto.UserDTO;

import java.util.List;

public interface UserService {
    UserDTO addUser(UserCreateDTO user);

    List<UserDTO> getUsers();

    UserDTO updateUser(UserCreateDTO user, Long id);

    void deleteUser(Long id);

}
