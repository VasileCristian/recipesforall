package com.example.finalproject.entity;

import com.example.finalproject.dto.Difficulty;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NonNull
    private String description;

    @NonNull
    private Integer preparationTime;

    @NonNull
    private Difficulty difficulty;

    private String instructions;

}
